import React from 'react';
import style from './style.module.scss';

const History = () => {
  return (
    <div>
      <h1 className={style['history__header']}>Our history</h1>
      <div className={style['history__teaser']}>
        Click on a thing to see the thing.
      </div>

      <div className={style['timeline']}>
        <div className={style['timeline__lines']}>
          <div
            className={`${style['timeline__height']} ${style['first']} ${style['lines']}`}
          >
            <div
              className={`${style['timeline__dot']} ${style['present']}`}
            ></div>
            <div
              className={`${style['timeline__dot']} ${style['year']} ${style['y2020']}`}
            ></div>
            <div className={style['timeline__dot job']}></div>
            <div className={style['timeline__line']}></div>
          </div>
        </div>
        <div className={style['timeline__labels']}>
          <div
            className={`${style['timeline__height']} ${style['first']} ${style['labels']}`}
          >
            <div className={`${style['timeline__label']} ${style['present']}`}>
              Present
            </div>
            <div className={style['timeline__label y2020']}>2020</div>
          </div>
        </div>
        <div className={style['timeline__brackets']}>
          <div className={style['timeline__height first brackets']}>
            <div className={style['timeline__bracket']}></div>
            <div className={style['timeline__bracket-blocker']}></div>
          </div>
        </div>
        <div className={style['timeline__titles']}>
          <div className={style['timeline__height first titles']}>
            <div className={style['timeline__title']}>Tampa Bay Times</div>
            <div className={style['timeline__description']}>UI Developer</div>
          </div>
        </div>
      </div>

      <div className={style['timeline']}>
        <div className={style['timeline__lines']}>
          <div className={style['timeline__height second lines']}>
            <div className={style['timeline__dot year y2020']}></div>
            <div className={style['timeline__dot job']}></div>
            <div className={style['timeline__line']}></div>
          </div>
        </div>
        <div className={style['timeline__labels']}>
          <div className={style['timeline__height second labels']}>
            <div className={style['timeline__label present']}>Present</div>
            <div className={style['timeline__label y2020']}>2019</div>
          </div>
        </div>
        <div className={style['timeline__brackets']}>
          <div className={style['timeline__height second brackets']}>
            <div className={style['timeline__bracket']}></div>
            <div className={style['timeline__bracket-blocker']}></div>
          </div>
        </div>
        <div className={style['timeline__titles']}>
          <div className={style['timeline__height second titles']}>
            <div className={style['timeline__title']}>FreightCenter</div>
            <div className={style['timeline__description']}>
              Senior Manager of Creative Development
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default History;
