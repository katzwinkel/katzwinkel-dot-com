import React from 'react';
import Container from './../components/ui/container/';
import Logo from './../public/svg/logo';
import KeyButton from './../components/ui/buttons/key-button/';
import debounce from 'lodash/debounce.js';
import Separator from './../components/ui/separators/';
import MobileScreen from './_home-children/mobile-screen/';
import LaptopScreen from './_home-children/laptop-screen/';
import NotKatzwinkel from './../components/segments/not-katzwinkel/';
import WiredSegment from './../components/segments/triptych/';
import BlenderBlurb from './../components/ui/icon-topped-blurb/';
import ToolsSegment from './../components/segments/icon-topped-panel/';
import DataToolsSegment from './../components/segments/accordion/';
import UnicornSegment from './../components/segments/icon-offset-card/';

import Layout from '../layout/';
import style from './styles.module.scss';

export default function Home() {
  const layoutMeta = {
    title: 'Jason Katzwinkel — UI Developer',
    description:
      'The website, resume and portfolio of Jason Katzwinkel, UI Developer.',
  };

  const dataToolsData = [
    {
      name: 'Google Analytics',
      exposition: 'Lorem ipsum dolor sit amet.',
    },
    {
      name: 'Google Search Console',
      exposition: 'Lorem ipsum dolor sit amet consectetur.',
    },
    {
      name: 'Google Optimize',
      exposition: 'Google Optimize Lorem ipsum dolor sit amet consectetur.',
    },
    {
      name: 'Google Data Studio',
      exposition: 'Google Data Studio Lorem ipsum dolor sit amet consectetur.',
    },
    {
      name: 'Parse.ly',
      exposition:
        'Parse.ly Lorem ipsum dolor sit amet consectetur. Parse.ly Lorem ipsum dolor sit amet consectetur. Parse.ly Lorem ipsum dolor sit amet consectetur. Parse.ly Lorem ipsum dolor sit amet consectetur. Parse.ly Lorem ipsum dolor sit amet consectetur. ',
    },
    {
      name: 'HotJar',
      exposition: 'HotJar Lorem ipsum dolor sit amet consectetur.',
    },
    {
      name: 'Metrics For News',
      exposition:
        'Metrics For News Lorem ipsum dolor sit amet consectetur. Metrics For News Lorem ipsum dolor sit amet consectetur. Metrics For News Lorem ipsum dolor sit amet consectetur. Metrics For News Lorem ipsum dolor sit amet consectetur. Metrics For News Lorem ipsum dolor sit amet consectetur. Metrics For News Lorem ipsum dolor sit amet consectetur. ',
    },
  ];

  return (
    <Layout layoutMeta={layoutMeta}>
      <div className={style['hero__overflow']}>
        <Container width={'twelve-eighty'}>
          <div className={style['hero']}>
            <div className={style['hero__panel--one']}>
              <Logo className={style['hero__logo']} />
              <h1 className={style['hero__header']}>
                A better workplace <span>starts</span> with a better{' '}
                <span>UI Developer</span>
              </h1>
              <p className={style['hero__teaser']}>
                Katzwinkel is a HaaS<span>*</span> platform with a customizable
                suite of cutting-edge features that will help take your company
                to the next level. With a focus on{' '}
                <strong>user interface design</strong> and{' '}
                <strong>development</strong>, Katzwinkel will bleep your blorps,
                so you don't have to.
              </p>
              <div className={style['hero__asterisk']}>
                <span>*</span> Human as a Service
              </div>
              <div className={style['hero__call-to-action']}>
                <KeyButton html={'Hire today!'} />
              </div>
            </div>
            <div className={style['hero__panel--two']}>
              <div className={style['hero__image']}></div>
              <MobileScreen />
              <LaptopScreen />
            </div>
          </div>
        </Container>
      </div>
      <Separator height={'128'} />
      <NotKatzwinkel />
      <Separator height={'32'} />
      <WiredSegment />
      <Separator height={'32'} />
      <BlenderBlurb
        icon={'/images/blender-logo.png'}
        iconAlt="Blender logo."
        iconURL={'https://www.blender.org/'}
      >
        <a href="https://www.blender.org/">Blender</a> is my tool of choice for
        3D modeling and rendering.
      </BlenderBlurb>
      <Separator height={'128'} />
      <ToolsSegment />
      <Separator height={'128'} />
      <DataToolsSegment data={dataToolsData} />
      <Separator height={'128'} />
      <UnicornSegment />
    </Layout>
  );
}
