import React from 'react';
import { useInView } from 'react-intersection-observer';
import BookOpen from './../../../public/svg/book-open';
import ChevronLeft from './../../../public/svg/chevron-left';
import Export from './../../../public/svg/export';
import LightningBolt from './../../../public/svg/lightning-bolt';
import Lock from './../../../public/svg/lock';
import NewScreen from './../../../public/svg/new-screen';
import Reload from './../../../public/svg/reload';
import Wifi from './../../../public/svg/wifi';
import MobileWebsite from './../mobile-website/';
import style from './style.module.scss';

const MobileScreen = () => {
  const { ref, inView, entry } = useInView({
    threshold: 0,
    initialInView: true,
  });

  return (
    <div ref={ref} className={style['mobile-screen']}>
      {inView && (
        <>
          <div className={style['safari__header']}>
            <div className={style['safari__info-bar']}>
              <div className={style['safari__signal']}>
                <div className={style['safari__signal-bar']}></div>
                <div className={style['safari__signal-bar']}></div>
                <div className={style['safari__signal-bar']}></div>
                <div className={style['safari__signal-bar']}></div>
                <div className={style['safari__signal-bar']}></div>
              </div>
              <div className={style['safari__wifi']}>
                <Wifi />
              </div>
              <div className={style['safari__time']}>11:45 PM</div>
              <div className={style['safari__charging']}>
                <LightningBolt />
              </div>
              <div className={style['safari__battery']}>
                <div className={style['safari__battery-shell']}>
                  <div className={style['safari__battery-status']}></div>
                </div>
                <div className={style['safari__battery-node']}></div>
              </div>
            </div>
            <div className={style['safari__address-bar']}>
              <div className={style['safari__reader']}>
                <div className={style['safari__reader--small']}>A</div>
                <div className={style['safari__reader--big']}>A</div>
              </div>
              <div className={style['safari__address']}>
                <div className={style['safari__lock']}>
                  <Lock />
                </div>
                <div className={style['safari__url']}>katzwinkel.com</div>
              </div>
              <div className={style['safari__reload']}>
                <Reload />
              </div>
            </div>
          </div>
          <div>
            <MobileWebsite />
          </div>
          <div className={style['safari__footer']}>
            <div>
              <ChevronLeft />
            </div>
            <div className={style['chevron-right']}>
              <ChevronLeft />
            </div>
            <div>
              <Export />
            </div>
            <div>
              <BookOpen />
            </div>
            <div>
              <NewScreen />
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default MobileScreen;
