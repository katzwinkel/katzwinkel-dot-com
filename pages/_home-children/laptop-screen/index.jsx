import React, { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import Logo from './../../../public/svg/logo';
import X from './../../../public/svg/x.jsx';
import ArrowLeft from './../../../public/svg/arrow-left.jsx';
import ArrowRight from './../../../public/svg/arrow-right.jsx';
import Reload from './../../../public/svg/reload.jsx';
import Lock from './../../../public/svg/lock.jsx';
import DotsVertical from './../../../public/svg/dots-vertical.jsx';
import LaptopWebsite from '../laptop-website/';
import style from './styles.module.scss';

const LaptopScreen = () => {
  const { ref, inView, entry } = useInView({
    threshold: 0,
    initialInView: true,
  });

  return (
    <div ref={ref} className={style['laptop-screen']}>
      {inView && (
        <div className={style['browser']}>
          <div className={style['browser__header']}>
            <div className={style['browser__tabs']}>
              <div className={style['browser__controls']}>
                <div
                  className={`${style['browser__dot']} ${style['one']}`}
                ></div>
                <div
                  className={`${style['browser__dot']} ${style['two']}`}
                ></div>
                <div
                  className={`${style['browser__dot']} ${style['three']}`}
                ></div>
              </div>
              <div className={style['browser__tab']}>
                <div className={style['browser__tab-end']}>
                  <div
                    className={`${style['browser__tab-curve']} ${style['left']}`}
                  ></div>
                </div>
                <div className={style['browser__tab-center']}>
                  <Logo className={style['browser__tab-logo']} />
                  <div className={style['browser__tab-title']}>
                    Jason Katzwinkel
                  </div>
                  <X className={style['browser__tab-close']} />
                </div>
                <div className={style['browser__tab-end']}>
                  <div
                    className={`${style['browser__tab-curve']} ${style['right']}`}
                  ></div>
                </div>
              </div>
            </div>
            <div className={style['browser__address']}>
              <ArrowLeft
                className={`${style['browser__address-icon']} ${style['arrow-left']}`}
              />
              <ArrowRight
                className={`${style['browser__address-icon']} ${style['arrow-right']}`}
              />
              <Reload
                className={`${style['browser__address-icon']} ${style['reload']}`}
              />
              <div className={style['browser__address-input']}>
                <Lock
                  className={`${style['browser__address-icon']} ${style['lock']}`}
                />
                katzwinkel.com
              </div>
              <DotsVertical
                className={`${style['browser__address-icon']} ${style['dots-vertical']}`}
              />
            </div>
          </div>
          <LaptopWebsite />
        </div>
      )}
    </div>
  );
};

export default LaptopScreen;
