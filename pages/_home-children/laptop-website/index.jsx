import React from 'react';
// import Logo from '../../../../../resources/svg/logo.jsx';
import Menu from './../../../public/svg/menu.jsx';
import CursorDefault from './../../../public/svg/cursor-default.jsx';
import style from './styles.module.scss';

const MiniWebsite = () => {
  const miniNav = [
    'Home',
    'Features',
    'Benefits',
    'Our history',
    'About this site',
    'Pricing',
    'Hire Today!',
  ].map((item, index) => {
    return (
      <div key={`mini-nav-item-${index}`} className={style['mini__nav-item']}>
        {item}
      </div>
    );
  });

  return (
    <div className={style['mini__website']}>
      <CursorDefault className={style['mini__cursor']} />
      <div className={style['mini__click-tap']}></div>
      <div className={style['mini__nav-bar']}>
        <Menu className={style['mini__menu']} />
      </div>
      <div className={style['mini__nav']}>
        <div className={`${style['mini__nav-item']}`}>
          <div
            className={`${style['mini__nav-item-text']} ${style['home']}`}
          ></div>
        </div>
        <div className={`${style['mini__nav-item']}`}>
          <div
            className={`${style['mini__nav-item-text']} ${style['features']}`}
          ></div>
        </div>
        <div className={`${style['mini__nav-item']}`}>
          <div
            className={`${style['mini__nav-item-text']} ${style['benefits']}`}
          ></div>
        </div>
        <div className={`${style['mini__nav-item']}`}>
          <div
            className={`${style['mini__nav-item-text']} ${style['our']}`}
          ></div>
          <div
            className={`${style['mini__nav-item-text']} ${style['history']}`}
          ></div>
        </div>
        <div className={`${style['mini__nav-item']}`}>
          <div
            className={`${style['mini__nav-item-text']} ${style['about']}`}
          ></div>
          <div
            className={`${style['mini__nav-item-text']} ${style['this']}`}
          ></div>
          <div
            className={`${style['mini__nav-item-text']} ${style['site']}`}
          ></div>
        </div>
        <div className={`${style['mini__nav-item']}`}>
          <div
            className={`${style['mini__nav-item-text']} ${style['pricing']}`}
          ></div>
        </div>
        <div className={`${style['mini__nav-item']}`}>
          <div
            className={`${style['mini__nav-item-text']} ${style['hire']}`}
          ></div>
          <div
            className={`${style['mini__nav-item-text']} ${style['today']}`}
          ></div>
        </div>
      </div>

      <div className={style['mini__content']}>
        <div className={`${style['mini__page']} ${style['home']}`}>
          Mini home page
        </div>
        <div className={`${style['mini__page']} ${style['contact']}`}>
          Mini contact page
        </div>
      </div>
    </div>
  );
};

export default MiniWebsite;
