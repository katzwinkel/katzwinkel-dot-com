import React, { useRef } from 'react';
import Layout from './../../layout/';
import Container from './../../components/ui/container/';
import PageHeader from './../../components/ui/page-header/';
import CATCHaREP from './../../public/svg/catcharep';
import HeartBroken from './../../public/svg/heart-broken';
import style from './style.module.scss';

const Contact = () => {
  const layoutMeta = {
    title: 'Contact',
    description: 'contact',
  };

  const catcharep = useRef(null);

  const handleChange = (test) => {
    if (catcharep.current) {
      catcharep.current.className =
        test === 'fail'
          ? style['catcharep']
          : `${style['catcharep']} ${style['show']}`;
    }
    document.getElementById('heartBorkened').className =
      test === 'fail'
        ? `${style['catcharep__heart-borkened']} ${style['break-it']}`
        : style['catcharep__heart-borkened'];
    setTimeout(() => {
      document.getElementById('heartBorkened').className =
        style['catcharep__heart-borkened'];
    }, 1500);
  };

  const RadioInput = (props) => {
    const { label, test, id } = props && props;
    const classname =
      test === 'pass'
        ? `${style['catcharep__quiz-radio']} ${style['pass']}`
        : style['catcharep__quiz-radio'];
    return (
      <label className={style['catcharep__quiz-choice']}>
        <input
          className={classname}
          type="radio"
          name="catcharepRadio"
          id={id}
          onChange={() => {
            handleChange(test);
          }}
        />{' '}
        {label}
      </label>
    );
  };

  return (
    <Layout layoutMeta={layoutMeta}>
      <Container width={'nine-sixty'}>
        <PageHeader>Contact</PageHeader>
        <div className={style['contact__wrapper']}>
          <div className={style['catcharep']} ref={catcharep}>
            <div className={style['contact__teaser']}>Are you a robot?</div>
            <h2>
              Are <span>UI</span> and <span>UX</span>
              <br />
              the same thing?
            </h2>

            <div className={style['catcharep__content']}>
              <div className={style['catcharep__quiz']}>
                <div
                  className={style['catcharep__heart-borkened']}
                  id="heartBorkened"
                >
                  <HeartBroken />
                </div>
                <form name="catcharepForm" id="catcharepForm">
                  <RadioInput
                    label={'Yeah, totally.'}
                    test={'fail'}
                    id={'radioOne'}
                  />
                  <RadioInput
                    label={'Absolutely not.'}
                    test={'pass'}
                    id={'radioTwo'}
                  />
                  <RadioInput
                    label={'I dunno...'}
                    test={'fail'}
                    id={'radioThree'}
                  />
                </form>
              </div>

              <div className={style['catcharep__logo']}>
                <CATCHaREP className={style['catcharep__icon']} />
                <div className={style['catcharep__logo-label']}>CATCHaREP</div>
              </div>
            </div>
          </div>
          <div className={style['contact__info']}>
            <h2 className={style['contact__name']}>Jason Katzwinkel</h2>
            <a href="tel:01-312-208-9518" className={style['contact__phone']}>
              (312) 208-9518
            </a>
            <a href="mailto:jason@katzwinkel.com" className="contact__email">
              jason@katzwinkel.com
            </a>
          </div>
        </div>
      </Container>
    </Layout>
  );
};

export default Contact;
