import React from 'react';
import Layout from './../../../../layout/';
import Container from './../../../../components/ui/container/';
import PageHeader from './../../../../components/ui/page-header/';
import Git from './../../../../public/svg/git';
import ChevronLeft from './../../../../public/svg/chevron-left';
import s from './../../s.module.scss';

const Breadcrumbs = (props) => {
  return props.bread.map((crumb, index) => {
    return (
      <>
        <a href={crumb.path}>{crumb.text}</a>
        {index < props.bread.length - 1 && <ChevronLeft />}
      </>
    );
  });
};

const Document = (props) => {
  const { documentProps: d, children } = props;
  const layoutMeta = {
    title: d.title,
    description: d.description,
  };
  const GitLink = () => {
    return d.git ? (
      <a href={d.git} target="_blank">
        <Git className={s.git} />
      </a>
    ) : null;
  };
  return (
    <Layout layoutMeta={layoutMeta}>
      <PageHeader></PageHeader>
      <Container width={'six-eighty'}>
        {d.breadcrumbs && (
          <div className={`${s.breadcrumbs}`}>
            <Breadcrumbs bread={d.breadcrumbs} />
          </div>
        )}
        <div className={s.documentation}>
          <h1>
            <span>{d.title}</span>
            <GitLink />
          </h1>
          {children}
        </div>
      </Container>
      <PageHeader></PageHeader>
    </Layout>
  );
};

export default Document;
