import React from 'react';
import Document from './../_components/layout/';
import s from './../s.module.scss';

const Page = () => {
  const documentProps = {
    title: 'Documentation',
    description: 'The documentation of the documentation. Pretty cool, eh?',
    git:
      'https://bitbucket.org/katzwinkel/katzwinkel-dot-com/src/master/pages/documentation/',
    breadcrumbs: [{ text: 'Documentation', path: '/documentation' }],
  };
  return (
    <Document documentProps={documentProps}>
      <p>The documentation of the documentation. Pretty sweet, eh?</p>
      <h3>Usage</h3>
      <p>To add a page to documentation:</p>
      <ol>
        <li>
          Add a new folder underneath: <code>/pages/documentation/</code>
          <ul>
            <li>
              Name that folder whatever you want the URL to be after{' '}
              <code>/documentation/</code>.
            </li>
            <ul>
              <li>Nest the folders as needed for sensible path hierarchy.</li>
            </ul>
          </ul>
        </li>
        <li>
          In that folder, add a new file: <code>index.jsx</code>
        </li>
        <li>
          In that file, import:
          <ul>
            <li>React</li>
            <li>
              the <code>{`<Document />`}</code> component
            </li>
            <li>
              The documentation style module file
              <ul>
                <li>
                  <div className={s.codeblock}>
                    <code>{`/pages/documentation/s.modules.scss`}</code>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>In that file, create and export a new component.</li>
        <li>
          In that component, add a new object:{' '}
          <code>{`const documentProps = { ... }`}</code>
        </li>
        <li>
          In that object, create some optional properties, and populate the
          values accordingly:
          <ul>
            <li>
              <code>{`title:`}</code>
              <ul>
                <li>Accepts a string.</li>
                <li>
                  This will be used as both the H1 and the meta title of the
                  page.
                </li>
              </ul>
            </li>
            <li>
              <code>{`description:`}</code>
              <ul>
                <li>Accepts a string.</li>
                <li>This will be used the meta description of the page.</li>
              </ul>
            </li>
            <li>
              <code>{`git:`}</code>
              <ul>
                <li>Accepts a string, ideally a URL.</li>
                <li>
                  Used properly, this will generate a link to the corresponding
                  repo folder.
                </li>
                <li>
                  The link will manifest as a Git logo SVG next to the H1.
                </li>
              </ul>
            </li>
            <li>
              <code>{`breadcrumbs:`}</code>
              <ul>
                <li>Accepts an array of objects.</li>
                <li>
                  <code>{`[{text: ' ... ', path: ' ... '}]`}</code>
                </li>
                <li>
                  Used properly, this will generate a breadcrumb trail at the
                  top of the page.
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          In that component, add the{' '}
          <code>{`<Document documentProps={documentProps} />`}</code> component.
        </li>
        <li>
          In the file, import any other components you might need for use in the
          page.
        </li>
      </ol>

      <h3>Example</h3>
      <div className={s.codeblock}>
        <code>
          {`import React from 'react';`}
          <br />
          {`import Document from './../_components/layout/';`}
          <br />
          {`import s from './../s.module.scss';`}
          <br />
          <br />
          {`const Page = (props) => {`}
          <br />
          &emsp;&emsp;{`const documentProps = {`}
          <br />
          &emsp;&emsp;&emsp;&emsp;{`title: 'Example page',`}
          <br />
          &emsp;&emsp;&emsp;&emsp;
          {`description: 'This is an example page',`}
          <br />
          &emsp;&emsp;&emsp;&emsp;
          {`git: ___URL_TO_BITBUCKET___,`}
          <br />
          &emsp;&emsp;&emsp;&emsp;
          {`breadcrumbs: [{ text: 'Documentation', path: '/documentation' }],`}
          <br />
          &emsp;&emsp;{`};`}
          <br />
          &emsp;&emsp;{`return (`}
          <br />
          &emsp;&emsp;&emsp;&emsp;{`<Document documentProps={documentProps}>`}
          <br />
          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
          {`<p>The example contents of the example page.</p>`}
          <br />
          &emsp;&emsp;&emsp;&emsp;{`</Document>`}
          <br />
          &emsp;&emsp;{`)`}
          <br />
          {`}`}
          <br />
          <br />
          {`export default Page;`}
          <br />
        </code>
      </div>
    </Document>
  );
};

export default Page;
