import Container from './../../components/ui/container/';
import Layout from './../../layout/';
import PageHeader from './../../components/ui/page-header/';
import Document from './_components/layout/index';

const Documentation = () => {
  const layoutMeta = {
    title: 'Documentation',
    description: 'uytgbnm',
  };
  const documentProps = {
    title: 'Documentation',
    description:
      'The documentation detailing the various components and functions of Katzwinkel.com.',
  };
  return (
    <Document documentProps={documentProps}>
      <ul>
        <li>
          <a href="/documentation/documentation">Documentation</a>
        </li>
        <li>
          <a href="/documentation/components">Components</a>
          <ul>
            <li>
              <a href="/documentation/components/container">
                <code>{`<Container />`}</code>
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="/documentation/functions">Functions</a>
          <ul>
            <li>
              <a href="/documentation/functions/roll-css-modules">
                <code>rollCSSmodules()</code>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </Document>
  );
};

export default Documentation;
