import React from 'react';
import Document from './../../_components/layout/';
import IconWithWord from './../../../../components/ui/icon-with-word/';
import More from './../../../../public/svg/more';
import s from './../../s.module.scss';

const Page = () => {
  const documentProps = {
    title: 'rollCSSmodules()',
    description:
      'Details and documentation regarding the rollCSSmodules() function.',
    git:
      'https://bitbucket.org/katzwinkel/katzwinkel-dot-com/src/master/utilities/js/functions.js',
    breadcrumbs: [
      { text: 'Documentation', path: '/documentation' },
      { text: 'Functions', path: '/documentation/functions' },
    ],
  };
  return (
    <Document documentProps={documentProps}>
      <p>
        <code>rollCSSmodules()</code> is a function that helps manage
        dynamically-populated CSS classes, for simple placement on an element.
        The <code>rollCSSmodules()</code> function concatenates CSS class
        modules that are:
      </p>
      <ul>
        <li>imported from the stylesheet in the component, and</li>
        <li>imported from the stylesheet in the parent component.</li>
      </ul>
      <h3>Usage</h3>
      <code>rollCSSmodules()</code> accepts three arguments:
      <ol>
        <li>A stylesheet: the stylesheet as imported by the component,</li>
        <li>
          An array of strings: any class names you’d like to apply to the
          element that exist in the component's stylesheet
        </li>
        <li>
          An array of strings: additional modules from the parent stylesheet
        </li>
      </ol>
      <h3>Output</h3>
      <p>
        <code>rollCSSmodules()</code> outputs a single string of CSS modules
        (separated by spaces).
      </p>
      <h3>Example</h3>
      <div className={s.codeblock}>
        <code>
          {`import s from './s.module.scss';`}
          <br />
          <br />
          {`<ParentComponent>`}
          <br />
          &emsp;&emsp;
          {`<ChildComponent color={'patina'} parentModules={[s.whatever]} />`}
          <br />
          {`</ParentComponent>`}
          <br />
        </code>
      </div>
      <hr />
      <div className={s.codeblock}>
        <code>
          import {`{ rollCSSmodules }`} from './../functions/';
          <br />
          import s from './s.module.scss';
          <br />
          <br />
          const {`{ color = null, parentModules = [] }`} = props;
          <br />
          const classnames = rollCSSmodules(s, ['ooh-fancy', color],
          parentModules);
          <br />
          <br />
          &lt;div className={`{classnames}`}&gt;&lt;/div&gt;
        </code>
      </div>
      <h3>Example output</h3>
      <div className={s.codeblock}>
        <code>
          {`<div class="s_ooh-fancy__32Xfj s_patina__Gn2fq s_whatever__DcOWh"></div>`}
        </code>
      </div>
      <p>
        The above example assumes that the classes <code>.ooh-fancy</code> and{' '}
        <code>.patina</code> are in the <code>ChildComponent</code>{' '}
        <code>s.module.scss</code> file, and that the class{' '}
        <code>.whatever</code> is in the <code>ParentComponent</code>{' '}
        <code>s.module.scss</code> file
      </p>
      <hr />
      <IconWithWord Icon={More} color="patina" parentModules={[s.infobox]}>
        Read more about CSS Modules
        <a
          href="https://create-react-app.dev/docs/adding-a-css-modules-stylesheet/"
          target="_blank"
        >
          here
        </a>
        .
      </IconWithWord>
    </Document>
  );
};

export default Page;
