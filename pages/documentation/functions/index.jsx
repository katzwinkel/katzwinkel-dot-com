import React from 'react';
import Document from './../_components/layout/';
import s from './../s.module.scss';

const Page = () => {
  const documentProps = {
    title: 'Functions',
    description:
      'The list of documents detailing the various functions of Katzwinkel.com.',
    breadcrumbs: [{ text: 'Documentation', path: '/documentation' }],
  };
  return (
    <Document documentProps={documentProps}>
      <ul>
        <li>
          <a href="/documentation/functions/roll-css-modules">
            <code>{`rollCSSmodules()`}</code>
          </a>
        </li>
      </ul>
    </Document>
  );
};

export default Page;
