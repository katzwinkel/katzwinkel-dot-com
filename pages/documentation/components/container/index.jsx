import React from 'react';
import Document from './../../_components/layout/';
import s from './../../s.module.scss';

const Page = () => {
  const documentProps = {
    title: 'Container',
    description:
      'Details and documentation regarding the <Container /> component.',
    git:
      'https://bitbucket.org/katzwinkel/katzwinkel-dot-com/src/master/components/ui/container/',
    breadcrumbs: [
      { text: 'Documentation', path: '/documentation' },
      { text: 'Components', path: '/documentation/components' },
    ],
  };
  return (
    <Document documentProps={documentProps}>
      <p>
        A "container" &mdash; in this context &mdash; is a basic element of page
        construction, consisting of a max-width <code>{`<div />`}</code>{' '}
        centered on the page. By default, a <code>{`<Container />`}</code>{' '}
        starts with a left and right margin of 1.6rem; that margin increases to
        3.2rem at a 420px breakpoint. Once the <code>{`<Container />`}</code>{' '}
        reaches its max-width, the margin switches to auto.
      </p>
      <h3>Usage</h3>
      <p>
        <code>{`<Container />`}</code> accepts the following props:
      </p>
      <ul>
        <li>
          <code>width</code>
          <ul>
            <li>
              <code>PropTypes.string</code>
            </li>
            <li>'six-eighty'</li>
            <li>'nine-sixty'</li>
            <li>'twelve-eighty'</li>
          </ul>
        </li>
        <li>
          <code>margin</code>
          <ul>
            <li>
              <code>PropTypes.string</code>
            </li>
            <li>'flush'</li>
            <li>
              <code>{`margin={'flush'}`}</code> eliminates the margins of the
              container until it reaches its max-width.
            </li>
          </ul>
        </li>
        <li>
          <code>children</code>
          <ul>
            <li>
              The <code>{`<Container />`}</code> outputs anything contained
              within its tags.
            </li>
          </ul>
        </li>
      </ul>
      <h3>Example usage</h3>
      <div className={s.codeblock}>
        <code>
          {`import Container from '/components/ui/container/';`}
          <br />
          <br />
          {`<Container width={'nine-sixty'} margin={'flush'}>`}
          <br />
          &emsp;&emsp;
          {`<p>This content will be output as the 'children' prop.</p>`}
          <br />
          {`</Container>`}
        </code>
      </div>
      <h3>Example output</h3>
      <div className={s.codeblock}>
        <code>
          {`<div class="s_container__32Xfj s_six-eighty__DcOWh s_flush__8U6gm">`}
          <br />
          &emsp;&emsp;
          {`<p>This content will be output as the 'children' prop.</p>`}
          <br />
          {`</div>`}
        </code>
      </div>
    </Document>
  );
};

export default Page;
