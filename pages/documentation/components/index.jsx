import React from 'react';
import Document from './../_components/layout/';
import s from './../s.module.scss';

const Page = () => {
  const documentProps = {
    title: 'Components',
    description:
      'The list of documents detailing the various components of Katzwinkel.com.',
    breadcrumbs: [{ text: 'Documentation', path: '/documentation' }],
  };
  return (
    <Document documentProps={documentProps}>
      <ul>
        <li>
          <a href="/documentation/components/container">
            <code>{`<Container />`}</code>
          </a>
        </li>
      </ul>
    </Document>
  );
};

export default Page;
