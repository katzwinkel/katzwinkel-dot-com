import React from 'react';
import Layout from './../../layout/';
import PageHeader from './../../components/ui/page-header/';
import CheckCircleOutline from './../../public/svg/check-circle-outline';
import CloseCircleOutline from './../../public/svg/close-circle-outline';
import Tag from './../../components/ui/tags/';
import FlatButton from './../../components/ui/buttons/flat-button/';

import style from './style.module.scss';

const Perk = (props) => {
  const { icon, html } = props;
  return (
    <div className={style['pricing__perk']}>
      {icon} <span dangerouslySetInnerHTML={{ __html: html }}></span>
    </div>
  );
};

const perksPartTime = [
  { html: 'Annual salary' },
  { html: 'Health and wellness' },
  { html: 'Generous paid leave' },
  { html: 'Remote' },
];

const perksFullTime = [
  { html: 'Annual salary' },
  { html: 'Health and wellness' },
  { html: 'Generous paid leave' },
  { html: 'Remote or on-site<span>*</span>' },
  { html: 'Career growth' },
  { html: 'Perks, and so much more!' },
];

const partTimePerks = perksPartTime.map((perk, index) => {
  return (
    <Perk
      key={`part-time-perks-${index}`}
      icon={<CloseCircleOutline />}
      html={perk.html}
    />
  );
});

const fullTimePerks = perksFullTime.map((perk, index) => {
  return (
    <Perk
      key={`full-time-perks-${index}`}
      icon={<CheckCircleOutline />}
      html={perk.html}
    />
  );
});

const Pricing = (props) => {
  const layoutMeta = {
    title: 'Pricing',
    description: 'description',
  };

  const { handleNavigation } = props;
  return (
    <Layout layoutMeta={layoutMeta}>
      <div className={style['pricing']}>
        <PageHeader>Pricing</PageHeader>
        <div className={style['pricing__teaser']}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s.
        </div>
        <div className={style['pricing__options']}>
          <div className={`${style['pricing__nope']} ${style['part-time']}`}>
            <div className={style['pricing__tag--center']}>
              <Tag>Discontinued</Tag>
            </div>
            <h2>Part time</h2>
            <div className={`${style['pricing__perks']} ${style['part-time']}`}>
              {partTimePerks}
            </div>
          </div>
          <div className={style['pricing__yep']}>
            <div className={style['pricing__tag--center']}>
              <Tag color={['tradewind']}>Recommended</Tag>
            </div>
            <h2>Full time</h2>
            <div className={`${style['pricing__perks']} ${style['full-time']}`}>
              {fullTimePerks}
            </div>
            <div className={style['pricing__call-to-action']}>
              <FlatButton url={'/contact'}>Hire today!</FlatButton>
            </div>
            <div className="pricing__asterisk">
              <span>*</span> On-site in Tampa Bay Area only.
            </div>
          </div>
          <div
            className={`${style['pricing__nope']} ${style['contract-to-hire']}`}
          >
            <div className={style['pricing__tag--center']}>
              <Tag>Out of stock</Tag>
            </div>
            <h2>Contract to hire</h2>
            <div className={`${style['pricing__perks']} ${style['part-time']}`}>
              {partTimePerks}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Pricing;
