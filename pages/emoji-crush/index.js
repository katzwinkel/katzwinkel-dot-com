import React, { useState, useRef, useEffect } from 'react';
import Container from '../../components/ui/container/index.js';
import Layout from '../../layout/index.js';
import style from './style.module.scss';

// https://www.npmjs.com/package/react-draggable
import Draggable from 'react-draggable';

const Emoji = ({ type }) => {
  return (
    <div className={`${style['emoji']} emoji`} data-type={type}>
      <img src={`/images/${type}.png`} height="100%" width="100%" />
    </div>
  );
};

const DraggableBox = (props) => {
  //***** START: Don't let an emoji drag outside the main grid.
  const { sequence, b } = props;
  // Default boundaries
  let top = -72,
    left = -72,
    right = 72,
    bottom = 72;

  // Edge boundaries
  switch (true) {
    case sequence === 0: // Top left corner
      (top = 0), (left = 0);
      break;
    case sequence > 0 && sequence < 7: // Top side
      top = 0;
      break;
    case sequence === 7: // Top right corner
      (top = 0), (right = 0);
      break;
    case sequence === 56: // Bottom left corner
      (left = 0), (bottom = 0);
      break;
    case sequence > 56 && sequence < 63: // Bottom side
      bottom = 0;
      break;
    case sequence === 63: // Bottom right corner
      (right = 0), (bottom = 0);
      break;
    case sequence % 8 === 0: // Right side
      left = 0;
      break;
    case sequence % 8 === 7: // Left side
      right = 0;
      break;
    default:
      break;
  }
  const bounds = { top: top, left: left, right: right, bottom: bottom };
  //***** END: Don't let an emoji drag outside the main grid.

  //***** START: Randomize emojis on grid.
  // https://emojipedia.org/apple/
  const colors = ['frown', 'grin', 'explode', 'poo', 'robot', 'scream'];
  const emoji = colors[Math.floor(Math.random() * colors.length)];
  //***** END: Randomize emojis on grid.

  //***** START: Handle dragging
  // Move an overlapped emoji into the place of the original.
  // See switch in handleDrag => function.
  function move(offset, x, y) {
    b[sequence + offset].current.querySelector(
      '.drag-wrapper'
    ).style.transform = `translate3d(${x}, ${y}, 0)`;
  }

  // Put any moved emojis back into their original position
  // when original emoji moves away.
  // See switch in handleDrag => function.
  function resetOthers(offsets) {
    offsets.map((offset) => {
      if (b[sequence + offset]) {
        b[sequence + offset].current
          .querySelector('.drag-wrapper')
          .removeAttribute('style');
      }
    });
  }

  const handleDrag = (e, ui) => {
    // If dragged emoji overlaps another emoji,
    // move the other emoji into the dragged emoji's spot.
    // If dragged emoji moves away from an overlapped emoji,
    // put it back where it was.
    switch (true) {
      //Top left box
      case ui.x < -50 && ui.y < -50:
        move(-9, '50px', '50px');
        resetOthers([-8, -7, -1, 1, 7, 8, 9]);
        break;
      //Top right box
      case ui.x > 50 && ui.y < -50:
        move(-7, '-94px', '50px');
        resetOthers([-9, -8, -1, 1, 7, 8, 9]);
        break;
      //Bottom left box
      case ui.x < -50 && ui.y > 50:
        move(7, '50px', '-94px');
        resetOthers([-9, -8, -7, -1, 1, 8, 9]);
        break;
      //Bottom right box
      case ui.x > 50 && ui.y > 50:
        move(9, '-94px', '-94px');
        resetOthers([-9, -8, -7, -1, 1, 7, 8]);
        break;
      // Top box
      case ui.y < -50:
        move(-8, '-50%', '50px');
        resetOthers([-9, -7, -1, 1, 7, 8, 9]);
        break;
      // Right box
      case ui.x > 50:
        move(1, '-94px', '-50%');
        resetOthers([-9, -8, -7, -1, 7, 8, 9]);
        break;
      // Bottom box
      case ui.y > 50:
        move(8, '-50%', '-94px');
        resetOthers([-9, -8, -7, -1, 1, 7, 9]);
        break;
      // Left box
      case ui.x < -50:
        move(-1, '50px', '-50%');
        resetOthers([-9, -8, -7, 1, 7, 8, 9]);
        break;
      default:
        resetOthers([-9, -8, -7, -1, 1, 7, 8, 9]);
    }
  };

  const handleStop = (e, ui) => {
    // If the dragged emoji stops while overlapping
    // another emoji, swap those two emoji's places.
    // Start the chain of events with an event.

    let swap = null;

    switch (true) {
      //Top left box
      case ui.x < -50 && ui.y < -50:
        swap = -9;
        break;
      //Top right box
      case ui.x > 50 && ui.y < -50:
        swap = -7;
        break;
      //Bottom left box
      case ui.x < -50 && ui.y > 50:
        swap = 7;
        break;
      //Bottom right box
      case ui.x > 50 && ui.y > 50:
        swap = 9;
        break;
      // Top box
      case ui.y < -50:
        swap = -8;
        break;
      // Right box
      case ui.x > 50:
        swap = 1;
        break;
      // Bottom box
      case ui.y > 50:
        swap = 8;
        break;
      // Left box
      case ui.x < -50:
        swap = -1;
        break;
      default:
        break;
    }
    if (swap) {
      const swapEvent = new CustomEvent('swapEmojis', {
        detail: { sequence: sequence, swap: swap },
      });
      window.dispatchEvent(swapEvent);
    }
  };
  //***** END: Handle dragging

  return (
    <div
      className={style['box']}
      ref={b[sequence]}
      data-sequence={sequence}
      data-fall={0}
    >
      <div className={`${style['drag-wrapper']} drag-wrapper`}>
        <Draggable
          // This position makes sure the emoji returns to its
          // original state if no valid moves are made,
          // and keeps the dragged emoji from wandering after stop.
          position={{ x: 0, y: 0 }}
          // Bounds keeps the emoji from being dragged too far.
          // See bounds const above.
          bounds={bounds}
          onDrag={(e, ui) => {
            handleDrag(e, ui);
          }}
          onStop={(e, ui) => {
            handleStop(e, ui);
          }}
        >
          <div>
            <Emoji type={emoji} />
          </div>
        </Draggable>
      </div>
    </div>
  );
};

const EmojiCrush = () => {
  // Populate page meta
  const layoutMeta = {
    title: 'Emoji Crush',
    description: 'I&rsquo;m trying to emulate a game.',
  };

  // Use an array to generate a reference for each of the boxes.
  // I can't believe this works.
  const b = [];
  for (let db = 0; db < 64; db += 1) {
    b[db] = useRef(null);
  }

  useEffect(() => {
    // Swap emojis between boxes.
    // See handleStop => function.
    const swapEmojis = (sequence, offset, callback) => {
      const box1 = b[sequence].current;
      const box2 = b[sequence + offset].current;
      const wrap1 = box1.querySelector('.drag-wrapper');
      const wrap2 = box2.querySelector('.drag-wrapper');
      const drag1 = box1.querySelector('.react-draggable');
      const drag2 = box2.querySelector('.react-draggable');
      const emoji1 = box1.querySelector('.emoji');
      const emoji2 = box2.querySelector('.emoji');
      const clonemoji1 = emoji1.cloneNode(true) || null;
      const clonemoji2 = emoji2.cloneNode(true) || null;
      // Prevent animation when moved swapBox returns
      // to make it seamless.
      wrap2.style.transition = 'none';
      // Reset transforms
      wrap1.removeAttribute('style');
      drag1.style.transform = 'translate(0px, 0)';
      wrap2.style.transform = 'translate3d(-50%, -50%, 0)';
      // Delete the original emojis from their boxes.
      emoji1.remove();
      emoji2.remove();
      // Plant transposed emoji clones into new boxes
      drag1.appendChild(clonemoji2);
      drag2.appendChild(clonemoji1);

      // After animation timing,
      // clear transition from .drag-wrappers
      // and then look for combos.
      setTimeout(() => {
        for (let l of b) {
          l.current.querySelector('.drag-wrapper').style.transition = null;
        }
        callback();
      }, 1);
    };

    //***** START: Check grid for combinations.
    const findCombos = (callback) => {
      for (let fc = 0; fc < b.length - 2; fc += 1) {
        const box1 = b[fc].current;
        const box2 = b[fc + 1].current;
        const box3 = b[fc + 2].current;
        if (fc % 8 < 6) {
          const emoji1 = box1.querySelector('.emoji')
            ? box1.querySelector('.emoji').dataset.type
            : 'empty';
          const emoji2 = box2.querySelector('.emoji')
            ? box2.querySelector('.emoji').dataset.type
            : 'empty';
          const emoji3 = box3.querySelector('.emoji')
            ? box3.querySelector('.emoji').dataset.type
            : 'empty';
          if (emoji1 === emoji2 && emoji2 === emoji3) {
            box1.querySelector('.react-draggable').innerHTML = '';
            box2.querySelector('.react-draggable').innerHTML = '';
            box3.querySelector('.react-draggable').innerHTML = '';
          }
        }
      }
      callback();
    };

    const fallDown = () => {
      // Cycle through the grid.
      // Flag any emoji that needs to fall.
      for (let element of b) {
        const box = element.current;
        const emoji = box.querySelector('.emoji') || null;
        const sequence = box.dataset.sequence || null;
        // If a box does not contain an emoji, add +1 fall to every emoji above it.
        if (!emoji) {
          for (let i = sequence - 8; i > -1; i -= 8) {
            // Only add to fall to boxes with emojis,
            // or it jacks everything up.
            if (b[i].current.querySelector('.emoji')) {
              const df = parseInt(b[i].current.dataset.fall);
              b[i].current.dataset.fall = df + 1;
            }
          }
        }
      }
      // Cycle again, and animate the fall.
      for (let l of b) {
        const box = l.current;
        const emoji = box.querySelector('.emoji') || null;
        const fall = box.dataset.fall || null;
        // If there is an emoji, and it needs to fall...
        if (emoji && fall > 0) {
          emoji.style.transform = `translateY(${fall * 72}px)`;
        }
      }
      // Wait until transition is complete,
      // then actually move the emojis to the appropriate box.
      setTimeout(() => {
        for (let l of b) {
          const box = l.current;
          const emoji = box.querySelector('.emoji') || null;
          const clonemoji = emoji ? emoji.cloneNode(true) : null;
          if (clonemoji) {
            clonemoji.style.transform = null;
          }
          const sequence = parseInt(box.dataset.sequence) || null;
          const fall = parseInt(box.dataset.fall) || null;
          // If there is an emoji, and it needs to fall...
          if (emoji && fall > 0) {
            const drag2 =
              b[sequence + fall * 8].current.querySelector(
                '.react-draggable'
              ) || null;
            // Reduce fall to zero, so emojis falling into it don't also fall.
            box.dataset.fall = 0;
            // Remove the emoji from the box.
            emoji.remove();
            // Put the clone emoji in the other box.
            drag2.appendChild(clonemoji);
          }
        }
        // Cycle again, and reset all data-fall to zero.
        for (let l of b) {
          l.current.dataset.fall = 0;
        }
      }, 333);
    };

    //***** END: Check grid for combinations.
    window.addEventListener('swapEmojis', (e) => {
      swapEmojis(e.detail.sequence, e.detail.swap, () =>
        findCombos(() => fallDown())
      );
    });
  }, [b]);

  // Create boxes in grid
  const boxes = [];
  for (let bx = 0; bx < 64; bx += 1) {
    boxes.push(<DraggableBox sequence={bx} b={b} />);
  }

  return (
    <Layout layoutMeta={layoutMeta}>
      <Container width={'nine-sixty'}>
        <div id="grid" className={style['grid']}>
          {boxes}
        </div>
      </Container>
    </Layout>
  );
};

export default EmojiCrush;
