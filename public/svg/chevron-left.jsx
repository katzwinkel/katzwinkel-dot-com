import React from 'react';

const ChevronLeft = (props) => {
  return (
    <svg {...props} viewBox="0 0 14 24">
      <path d="M14 21.16L5.34683 12L14 2.82L11.336 0L0 12L11.336 24L14 21.16Z" />
    </svg>
  );
};

export default ChevronLeft;
