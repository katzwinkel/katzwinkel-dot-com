import React from 'react';

const ChevronDown = (props) => {
  return (
    <svg {...props} viewBox="0 0 24 14">
      <path d="M21.16 0L12 8.65317L2.82 0L0 2.66397L12 14L24 2.66397L21.16 0Z" />
    </svg>
  );
};

export default ChevronDown;
