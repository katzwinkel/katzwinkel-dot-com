import React from 'react';

const ArrowLeft = (props) => {
  return (
    <svg {...props} viewBox="0 0 24 24">
      <path d="M24 13.5152V10.4848H5.81818L14.1515 2.15152L12 0L0 12L12 24L14.1515 21.8485L5.81818 13.5152H24Z" />
    </svg>
  );
};

export default ArrowLeft;
