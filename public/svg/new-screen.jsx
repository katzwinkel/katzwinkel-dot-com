import React from 'react';

const NewScreen = (props) => {
  return (
    <svg {...props} viewBox="0 0 24 24">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M18 6V2C18 0.895431 17.1046 0 16 0H2C0.895431 0 0 0.89543 0 2V16C0 17.1046 0.89543 18 2 18H6V22C6 23.1046 6.89543 24 8 24H22C23.1046 24 24 23.1046 24 22V8C24 6.89543 23.1046 6 22 6H18ZM16 2H2L2 16H6V8C6 6.89543 6.89543 6 8 6H16V2ZM8 8H22V22H8V8Z"
      />
    </svg>
  );
};

export default NewScreen;
