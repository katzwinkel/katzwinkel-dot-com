import React from 'react';

const Logo = (props) => {
  return (
    <svg {...props} viewBox="0 0 140 80">
      <path d="M100 20H80L40 60H60L100 20Z" />
      <path d="M40 20V0L0 40L40 80V60L20 40L40 20Z" />
      <path d="M100 60V80L140 40L100 0V20L120 40L100 60Z" />
    </svg>
  );
};

export default Logo;
