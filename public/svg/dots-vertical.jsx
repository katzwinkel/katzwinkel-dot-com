import React from 'react';

const DotsVertical = props => {
  return (
    <svg {...props} viewBox="0 0 24 24">
      <circle cx="12" cy="12" r="3" />
      <circle cx="12" cy="3" r="3" />
      <circle cx="12" cy="21" r="3" />
    </svg>
  );
};

export default DotsVertical;
