import React from 'react';

const StarFourPoints = (props) => {
  return (
    <svg {...props} viewBox="0 0 24 24">
      <path d="M12 0L8.72727 8.72727L0 12L8.72727 15.2727L12 24L15.2727 15.2727L24 12L15.2727 8.72727L12 0Z" />
    </svg>
  );
};

export default StarFourPoints;
