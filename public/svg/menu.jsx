import React from 'react';

const Menu = props => {
  return (
    <svg {...props} viewBox="0 0 24 24">
      <path d="M0 4H24V6.66667H0V4ZM0 10.6667H24V13.3333H0V10.6667ZM0 17.3333H24V20H0V17.3333Z" />
    </svg>
  );
};

export default Menu;
