import React from 'react';

const HeartBroken = (props) => {
  return (
    <svg {...props} viewBox="0 0 24 24">
      <path d="M12 23.02L10.26 21.436C4.08 15.832 0 12.124 0 7.6C0 3.892 2.904 1 6.6 1C7.404 1 8.184 1.144 8.928 1.396L13.2 8.62L8.4 14.62L12 23.02ZM17.4 1C21.096 1 24 3.892 24 7.6C24 12.124 19.92 15.832 13.74 21.436L12 23.02L10.8 14.62L16.2 8.62L13.02 2.524C14.244 1.564 15.804 1 17.4 1Z" />
    </svg>
  );
};

export default HeartBroken;
