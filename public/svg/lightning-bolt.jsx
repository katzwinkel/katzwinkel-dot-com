import React from 'react';

const LightningBolt = (props) => {
  return (
    <svg {...props} viewBox="0 0 12 24">
      <path d="M5 15.2727H0L7 0V8.72727H12L5 24V15.2727Z" />
    </svg>
  );
};

export default LightningBolt;
