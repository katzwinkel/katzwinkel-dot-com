import React from 'react';
import style from './style.module.scss';

const MenuListItems = (props) => {
  const { page, html } = props;
  return (
    <li className={style['menu__list-item']}>
      <a
        href={page}
        className={style['menu__link']}
        dangerouslySetInnerHTML={{ __html: html }}
      ></a>
      <div className={style['menu__underline']}></div>
    </li>
  );
};

const Panel = (props) => {
  const { toggle } = props;
  const classes = toggle
    ? `${style['panel']} ${style['opened']}`
    : style['panel'];

  const menuList = [
    { page: '/', html: 'Home' },
    { page: '/features', html: 'Features' },
    { page: '/benefits', html: 'Benefits' },
    { page: '/history', html: 'Our history' },
    { page: '/about', html: 'About this site' },
    { page: '/pricing', html: 'Pricing' },
    { page: '/contact', html: 'Hire today!' },
  ];

  const menu = menuList.map((menuItem, index) => {
    return (
      <MenuListItems key={index} page={menuItem.page} html={menuItem.html} />
    );
  });

  return (
    <div className={classes}>
      <ul className={style['menu']}>{menu}</ul>
    </div>
  );
};

export default Panel;
