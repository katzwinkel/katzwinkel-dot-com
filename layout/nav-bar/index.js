import React, { useState } from 'react';
import style from './style.module.scss';
import Toggle from './toggle/';
import Panel from './panel/';

const NavBar = () => {
  const [toggle, setToggle] = useState(false);

  return (
    <React.Fragment>
      <div className={style['nav-bar']}>
        <Toggle
          toggle={toggle}
          handleToggle={() => {
            setToggle((t) => !t);
          }}
        />
      </div>
      <Panel toggle={toggle} />
    </React.Fragment>
  );
};

export default NavBar;
