import React from 'react';
import style from './style.module.scss';

const Toggle = (props) => {
  const { toggle, handleToggle } = props;
  let classes = style['toggle'];
  classes += toggle ? ` ${style['opened']}` : ``;
  return (
    <div id="menuToggle" className={classes} onClick={handleToggle}>
      <div className={style['toggle__slider']}>
        <div className={style['toggle__close']}>close</div>
        <div className={style['toggle__peg']}>
          <div className={style['toggle__peg-notch']}></div>
          <div className={style['toggle__peg-notch']}></div>
          <div className={style['toggle__peg-notch']}></div>
        </div>
        <div className={style['toggle__menu']}>menu</div>
      </div>
    </div>
  );
};

export default Toggle;
