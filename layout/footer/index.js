import React from 'react';
import PropTypes from 'prop-types';
import Container from './../../components/ui/container/';
import Logo from './../../public/svg/logo';
import FlatButton from './../../components/ui/buttons/flat-button/';
import Facebook from './../../public/svg/facebook';
import Twitter from './../../public/svg/twitter';
import Instagram from './../../public/svg/instagram';
import Pinterest from './../../public/svg/pinterest';
import Linkedin from './../../public/svg/linkedin';
import Poo from './../../public/svg/poo';
import Pi from './../../public/svg/pi';
import style from './style.module.scss';

const Footer = () => {
  const thisYear = new Date().getFullYear();
  return (
    <Container
      width={'full-width'}
      margin={'flush'}
      parentModules={[style['footer']]}
    >
      {/* https://www.youtube.com/watch?v=pXPXMxsXT28 */}
      <div className={style['pi']}>
        <Pi />
      </div>
      <Container width={'nine-sixty'} margin={'flush'}>
        <div className={style['logo-and-consultation']}>
          <div className={style['logo-and-tag']}>
            <Logo className={style['logo']} />
            <div className={style['logo-tag']}>Katzwinkel</div>
          </div>
          <div></div>
          <div className={style['schedule-an-interview']}>
            <div className={style['schedule-an-interview-label']}>
              Schedule an interview:
            </div>
            <div>
              <FlatButton url={'/contact'} color={['patina']}>
                Let's talk!
              </FlatButton>
            </div>
          </div>
        </div>
        <div className={style['link-bank']}>links</div>
        <div className={style['copyright']}>
          <div className={style['privacy']}>
            <span>Copyright &copy;{thisYear}</span>
            <a href="/privacy">Privacy Policy</a>
            <a href="/terms">Terms of Service</a>
          </div>
          <div></div>
          <div className={style['social']}>
            <a href="https://www.facebook.com" target="_blank">
              <Facebook />
            </a>
            <a href="https://www.twitter.com" target="_blank">
              <Twitter />
            </a>
            <a href="https://www.instagram.com" target="_blank">
              <Instagram />
            </a>
            <a href="https://www.pinterest.com" target="_blank">
              <Pinterest />
            </a>
            <a href="https://www.linkedin.com" target="_blank">
              <Linkedin />
            </a>
            <a href="#" target="_blank">
              <Poo />
            </a>
          </div>
        </div>
      </Container>
    </Container>
  );
};

export default Footer;
