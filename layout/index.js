import Head from 'next/head';
import PropTypes from 'prop-types';
import NavBar from './nav-bar';
import Footer from './footer/';
import style from './style.module.scss';

const Layout = (props) => {
  const {
    children,
    layoutMeta: { title, description },
  } = props;
  return (
    <div id="layout">
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar />
      <main>
        <div className={style['body']}>{children}</div>
      </main>
      <Footer />
    </div>
  );
};

Layout.propTypes = {
  layoutMeta: PropTypes.object,
};

export default Layout;
