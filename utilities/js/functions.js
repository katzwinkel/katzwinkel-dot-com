// rollCSSmodules() documentation:
// https://www.katzwinkel.com/documentation/functions/roll-css-module
export const rollCSSmodules = (a, b, c) => {
  return (Array.isArray(b) ? b.map((d) => a[d]) : null)
    .concat(Array.isArray(c) ? c : null)
    .filter(Boolean)
    .join(' ')
    .trim();
};
