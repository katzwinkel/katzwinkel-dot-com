import React from 'react';
import style from './style.module.scss';
import Container from './../../ui/container/';
import HammerScrewdriver from './../../../public/svg/hammer-screwdriver';
import IconWithWord from './../../ui/icon-with-word/';
import Fire from './../../../public/svg/fire';
import DotsHorizontal from './../../../public/svg/dots-horizontal';
import HumanCane from './../../../public/svg/human-cane';

const IconToppedPanel = () => {
  const propsOne = { icon: Fire, color: 'tan-hide' };
  const propsTwo = { icon: DotsHorizontal, color: 'patina' };
  const propsThree = { icon: HumanCane, color: 'grey999' };
  const iconsDataOne = [
    {
      text: 'HTML5',
      ...propsOne,
    },
    {
      text: 'CSS3',
      ...propsOne,
    },
    {
      text: 'ECMAScript 2015+',
      ...propsOne,
    },
    {
      text: 'SASS',
      ...propsOne,
    },
    {
      text: 'NodeJS / npm',
      ...propsOne,
    },
    {
      text: 'React',
      ...propsOne,
    },
    {
      text: 'NextJS',
      ...propsOne,
    },
    {
      text: 'SVG',
      ...propsOne,
    },
    {
      text: 'webpack',
      ...propsOne,
    },
    {
      text: 'Git',
      ...propsOne,
    },
    {
      text: 'Bitbucket',
      url: 'https://bitbucket.org/',
      ...propsOne,
    },
    {
      text: 'VSCode',
      ...propsOne,
    },
  ];

  const iconsDataTwo = [
    {
      text: 'Strapi',
      url: 'https://strapi.io/',
      ...propsTwo,
    },
    {
      text: 'Docker',
      ...propsTwo,
    },
    {
      text: 'Postman',
      ...propsTwo,
    },
    {
      text: 'Jira',
      ...propsTwo,
    },
    { text: 'tinyPNG', url: 'https://tinypng.com/', ...propsTwo },
  ];

  const iconsDataThree = [
    {
      text: 'jQuery',
      ...propsThree,
    },
    {
      text: 'PHP',
      ...propsThree,
    },
    {
      text: 'MySQL',
      ...propsThree,
    },
    {
      text: 'Gulp',
      ...propsThree,
    },
    {
      text: 'Java',
      ...propsThree,
    },
    {
      text: 'WordPress',
      ...propsThree,
    },
    {
      text: 'Bootstrap',
      ...propsThree,
    },
    {
      text: 'SVN',
      ...propsThree,
    },
    {
      text: 'Dreamweaver',
      ...propsThree,
    },
  ];

  const tools = (data) => {
    return data.map((data, index) => {
      return (
        <IconWithWord
          key={`icons-set-one-${index}`}
          url={data.url}
          Icon={data.icon}
          color={data.color}
          parentModules={[style.icon_word]}
        >
          {data.text}
        </IconWithWord>
      );
    });
  };

  return (
    <Container width={'nine-sixty'}>
      <div className={style['panel']}>
        <div className={style['icon-box']}>
          <HammerScrewdriver className={style['icon']} />
        </div>
        <h2 className={style['header--h2']}>Coding tools</h2>
        <h3 className={style['header--h3']}>This website</h3>
        <hr className={style['hr']} />
        <div className={style['grid']}>{tools(iconsDataOne)}</div>
        <h3 className={style['header--h3']}>Other</h3>
        <hr className={style['hr']} />
        <div className={style['grid']}>{tools(iconsDataTwo)}</div>
        <h3 className={style['header--h3']}>Back when</h3>
        <hr className={style['hr']} />
        <div className={style['grid']}>{tools(iconsDataThree)}</div>
      </div>
    </Container>
  );
};

export default IconToppedPanel;
