import React from 'react';
import { useInView } from 'react-intersection-observer';
import Container from './../../ui/container/';
import Unicorn from './_children/unicorn/';
import style from './style.module.scss';

const IconOffsetCard = () => {
  const { ref, inView, entry } = useInView({
    threshold: 0,
  });

  return (
    <Container width={'nine-sixty'} margin={'flush'}>
      <div className={style['background']}>
        <div ref={ref} className={style['decoration']}>
          {inView && <Unicorn inView={inView} />}
        </div>
        <div className={style['exposition']}>
          <h2>Designer. Programmer.</h2>
          <h3>
            Is Katzwinkel a <span>unicorn</span>?
          </h3>
          <p>
            Some say that's impossible; that there's no such thing as unicorns.
            To them, I say, "The only thing that's impossible is not{' '}
            <strong>believing</strong> in the impossible." And, maybe —
            <em>just maybe</em> — if I talk in nonsensical circles long enough,
            somebody might come out the other side reconsidering their position
            on unicorns.
          </p>
          <p>And that's when I know I'm making a difference.</p>
          <small>#marketing</small>
        </div>
      </div>
    </Container>
  );
};

export default IconOffsetCard;
