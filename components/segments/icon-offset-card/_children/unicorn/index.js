import React from 'react';
import PropTypes from 'prop-types';
import UnicornHead from './../../../../../public/svg/unicorn';
import StarFourPoints from './../../../../../public/svg/star-four-points';
import style from './style.module.scss';

const Unicorn = (props) => {
  const { inView } = props;
  return (
    <div
      className={
        inView ? `${style['spinner']} ${style['animate']}` : `${style['spin']}`
      }
    >
      <UnicornHead className={style['unicorn']} />
      <StarFourPoints className={`${style['star']} ${style['one']}`} />
      <StarFourPoints className={`${style['star']} ${style['two']}`} />
      <StarFourPoints className={`${style['star']} ${style['three']}`} />
      <StarFourPoints className={`${style['star']} ${style['four']}`} />
      <StarFourPoints className={`${style['star']} ${style['five']}`} />
      <StarFourPoints className={`${style['star']} ${style['six']}`} />
      <StarFourPoints className={`${style['star']} ${style['seven']}`} />
    </div>
  );
};

export default Unicorn;
