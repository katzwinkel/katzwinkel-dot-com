import React from 'react';
import Container from './../../ui/container/';
import Tag from './../../ui/tags/';
import style from './style.module.scss';

const Triptych = () => {
  return (
    <Container width={'nine-sixty'}>
      <h2 className={style['header']}>Design tools</h2>
      <div className={style['triptych']}>
        <div className={`${style['panel']} ${style['wired']}`}>
          <Tag color={['tradewind']}>Wired</Tag>
          <img src="/images/figma-logo.png" alt="Figma logo" />
          <h4>
            <a href="https://www.figma.com/" target="_blank">
              Figma
            </a>
          </h4>
          <p>
            <a href="https://www.figma.com/" target="_blank">
              Figma
            </a>{' '}
            is the web prototyping tool I enjoy the most. I just wish the image
            manipulation was better.
          </p>
        </div>
        <div className={`${style['panel']} ${style['tired']}`}>
          <Tag color={['grey666']}>Tired</Tag>
          <img src="/images/sketch-logo.png" alt="Sketch logo" />
          <h4>
            <a href="https://www.sketch.com/" target="_blank">
              Sketch
            </a>
          </h4>
          <p>
            <a href="https://www.sketch.com/" target="_blank">
              Sketch
            </a>{' '}
            is a fine tool, but one day I decided to give Figma a shot, and I
            haven't looked back since.
          </p>
        </div>
        <div className={`${style['panel']} ${style['expired']}`}>
          <Tag>Expired</Tag>
          <img src="/images/fireworks-logo.png" alt="Adobe Fireworks logo" />
          <h4>
            <a
              href="https://www.adobe.com/ca/products/fireworks.html"
              target="_blank"
            >
              Fireworks
            </a>
          </h4>
          <p>
            <a
              href="https://www.adobe.com/ca/products/fireworks.html"
              target="_blank"
            >
              Adobe Fireworks
            </a>{' '}
            was the greatest web design tool that ever was. Alas, it has been
            deprecated.
          </p>
        </div>
      </div>
    </Container>
  );
};

export default Triptych;
