import React from 'react';
import Container from './../../ui/container/';
import style from './style.module.scss';

const NotKatzwinkel = () => {
  return (
    <Container width={'nine-sixty'} margin={'flush'}>
      <div className={style['background']}>
        <a
          className={style['credit']}
          target="_blank"
          href="https://unsplash.com/@gurysimrat?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText"
        >
          <span>Photo credit</span>
        </a>
      </div>
      <div className={style['panel']}>
        <h2>
          Isn't it time <span>you</span> hired for&nbsp;<span>you</span>?
        </h2>
        <p>
          With Katzwinkel, you can take comfort in knowing that your boss will
          take comfort in knowing that your company's brand and codebase are in
          capable hands. Katzwinkel's quality design and professional
          codesmanship will allow you to take the credit you so richly deserve
          for finding this gem.
        </p>
        <small>
          <sub>*</sub> Not actually Katzwinkel.
          Results&nbsp;may&nbsp;vary.&nbsp;#marketing
        </small>
      </div>
    </Container>
  );
};

export default NotKatzwinkel;
