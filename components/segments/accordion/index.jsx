import React from 'react';
import useWindowSize from './../../../utilities/hooks/useWindowSize';
import Container from './../../ui/container/';
import Vertical from './_children/vertical/';
import Horizontal from './_children/horizontal/';

const Accordion = (props) => {
  const { data } = props;
  return (
    <Container width={'nine-sixty'}>
      <div>
        {useWindowSize().width < 640 ? (
          <Vertical data={data} />
        ) : (
          <Horizontal data={data} />
        )}
      </div>
    </Container>
  );
};

export default Accordion;
