import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';

const Horizontal = (props) => {
  const { data = [] } = props;

  // Generate a ref for each slat.
  const slats = data.map(() => {
    return useRef(null);
  });
  // Generate a ref for each exposition container.
  const expositions = data.map(() => {
    return useRef(null);
  });
  // Generate a ref for each exposition text height.
  const expositionTexts = data.map(() => {
    return useRef(null);
  });

  const handleClick = (index) => {
    // Does the clicked slat have an "active" class on it?
    const isActive = slats[index].current.classList.contains(style['active']);
    // Remove 'active' class from everything.
    slats.forEach((slat) => {
      slat.current.classList.remove(style['active']);
    });
    // Set all exposition heights to zero.
    expositions.forEach((exposition) => {
      exposition.current.classList.remove(style['active']);
    });
    // If clicked slat didn't already have "active" class...
    if (!isActive) {
      // ... add the "active" class to the slat and exposition.
      slats[index].current.classList.add(style['active']);
      expositions[index].current.classList.add(style['active']);
    }
  };

  const accordion = data
    ? data.map((slat, index) => {
        return (
          <div
            key={`vertical-accordion-slat-${index}`}
            ref={slats[index]}
            className={style['slat']}
            onClick={() => {
              handleClick(index);
            }}
          >
            <div className={style['name']}>
              <span>{slat.name}</span>
              <div className={style['magic-arrow']}>
                <div className={style['magic-arrow--line']}></div>
                <div className={style['magic-arrow--upper']}></div>
                <div className={style['magic-arrow--lower']}></div>
              </div>
            </div>
          </div>
        );
      })
    : [];

  const explanations = data
    ? data.map((explanation, index) => {
        return (
          <div ref={expositions[index]} className={style['exposition']}>
            {explanation.exposition}
          </div>
        );
      })
    : [];

  return (
    <div className={style['grid']}>
      <div>{accordion}</div>
      <div>
        <h2 className={style['heading']}>Data Tools</h2>
        <div className={style['expositions']}>
          <div className={style['instruction']}>
            Select a data tool on the left to read more about it.
          </div>
          {explanations}
        </div>
      </div>
    </div>
  );
};

Horizontal.propTypes = {
  data: PropTypes.array,
};

export default Horizontal;
