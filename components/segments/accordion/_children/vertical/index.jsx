import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import ChevronLeft from './../../../../../public/svg/chevron-left';

const Vertical = (props) => {
  const { data = [] } = props;

  // Generate a ref for each slat.
  const slats = data.map(() => {
    return useRef(null);
  });
  // Generate a ref for each exposition container.
  const expositions = data.map(() => {
    return useRef(null);
  });
  // Generate a ref for each exposition text height.
  const expositionTexts = data.map(() => {
    return useRef(null);
  });

  const handleClick = (index) => {
    // Does the clicked slat have an "active" class on it?
    const isActive = slats[index].current.classList.contains(style['active']);
    // Remove 'active' class from everything.
    slats.forEach((slat) => {
      slat.current.classList.remove(style['active']);
    });
    // Set all exposition heights to zero.
    expositions.forEach((exposition) => {
      exposition.current.style.height = 0;
    });
    // If clicked slat didn't already have "active" class...
    if (!isActive) {
      // ... add the "active" class to the slat.
      slats[index].current.classList.add(style['active']);
      // ... add the height of the text element to the expositions container.
      expositions[index].current.setAttribute(
        'style',
        `height: ${
          expositionTexts[index].current.getBoundingClientRect().height
        }px`
      );
    }
  };

  const accordion = data
    ? data.map((slat, index) => {
        return (
          <div
            key={`vertical-accordion-slat-${index}`}
            ref={slats[index]}
            className={style['slat']}
            onClick={() => {
              handleClick(index);
            }}
          >
            <div className={style['name']}>
              <span>{slat.name}</span>
              <ChevronLeft className={style['chevron']} />
            </div>
            <div ref={expositions[index]} className={style['exposition']}>
              <span ref={expositionTexts[index]}>{slat.exposition}</span>
            </div>
          </div>
        );
      })
    : [];

  return <>{accordion}</>;
};

Vertical.propTypes = {
  data: PropTypes.array,
};

export default Vertical;
