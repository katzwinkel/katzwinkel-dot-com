import React from 'react';
import PropTypes from 'prop-types';
import { rollCSSmodules } from './../../../../utilities/js/functions';
import style from './style.module.scss';

const FlatButton = (props) => {
  const { url = null, color = ['bittersweet'], children = [] } = props;
  const Tag = url ? 'a' : 'span';
  const classnames = rollCSSmodules(style, ['flat-button', color]);
  return (
    <Tag href={url} className={classnames}>
      <span dangerouslySetInnerHTML={{ __html: children }}></span>
    </Tag>
  );
};

FlatButton.propTypes = {
  url: PropTypes.string,
  color: PropTypes.array,
};

export default FlatButton;
