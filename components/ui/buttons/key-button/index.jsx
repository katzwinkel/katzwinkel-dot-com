import React from 'react';
import style from './style.module.scss';

const KeyButton = (props) => {
  const { html } = props;
  return (
    <div className={style['key-button']}>
      <div className={style['key-button__cover']}>
        {html && <span dangerouslySetInnerHTML={{ __html: html }}></span>}
      </div>
      <div className={style['key-button__base']}></div>
    </div>
  );
};

export default KeyButton;
