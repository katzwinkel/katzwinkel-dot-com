import React from 'react';
import PropTypes from 'prop-types';
import { rollCSSmodules } from './../../../utilities/js/functions';
import style from './style.module.scss';

const Tag = (props) => {
  const { color = [], children } = props;
  const classnames = rollCSSmodules(style, ['tag', color]);
  return (
    <div className={classnames}>
      {children && <span dangerouslySetInnerHTML={{ __html: children }}></span>}
    </div>
  );
};

Tag.propTypes = {
  color: PropTypes.array,
};

export default Tag;
