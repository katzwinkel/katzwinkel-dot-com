import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';

const Separator = (props) => {
  const { height } = props;
  return <div className={style[`h${height}`]}></div>;
};

Separator.propTypes = {
  CSSclass: PropTypes.string,
};

export default Separator;
