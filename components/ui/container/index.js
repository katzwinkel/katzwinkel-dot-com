import React from 'react';
import PropTypes from 'prop-types';
import { rollCSSmodules } from './../../../utilities/js/functions';
import style from './style.module.scss';

const Container = (props) => {
  const { width = null, margin = null, parentModules = [], children } = props;
  const classnames = rollCSSmodules(
    style,
    ['container', width, margin],
    parentModules
  );
  return <div className={classnames}>{children}</div>;
};

Container.propTypes = {
  width: PropTypes.string,
  margin: PropTypes.string,
  parentModules: PropTypes.array,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.func,
  ]),
};

export default Container;
