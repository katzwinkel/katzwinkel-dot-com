import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import Container from './../../ui/container/';

const IconToppedBlurb = (props) => {
  const { icon = null, iconAlt = null, iconURL = null, children = [] } = props;
  const Tag = iconURL ? 'a' : 'div';
  return (
    <Container width={'nine-sixty'}>
      <div className={style['grid']}>
        <Tag href={iconURL} className={style['icon']}>
          <img src={icon} alt={iconAlt} height="36" width="auto" />
        </Tag>
        <p className={style['children']}>{children}</p>
      </div>
    </Container>
  );
};

IconToppedBlurb.propTypes = {
  icon: PropTypes.string,
  iconURL: PropTypes.string,
  iconAlt: PropTypes.string,
};

export default IconToppedBlurb;
