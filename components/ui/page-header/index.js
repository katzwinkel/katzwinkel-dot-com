import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';

const PageHeader = (props) => {
  const { children = null } = props;
  return (
    <h1
      className={style['page-header']}
      dangerouslySetInnerHTML={{ __html: children }}
    />
  );
};

PageHeader.propTypes = {
  chidlren: PropTypes.object,
};

export default PageHeader;
