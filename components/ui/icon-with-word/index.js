import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import Link from './../../../public/svg/link';
import { rollCSSmodules } from './../../../utilities/js/functions';

const IconWithWord = (props) => {
  const { Icon, color = null, url = null, parentModules, children } = props;
  const Tag = url ? 'a' : 'div';
  const tagClassnames = rollCSSmodules(
    style,
    ['icon-with-words', url ? ['with-link'] : []],
    parentModules
  );
  const iconClassnames = rollCSSmodules(style, ['icon', color]);
  return (
    <Tag href={url} target={url ? '_blank' : null} className={tagClassnames}>
      <Icon className={iconClassnames} />
      <div className={style['words']}>{children}</div>
      {url && <Link className={style['link']} />}
    </Tag>
  );
};

IconWithWord.propTypes = {
  Icon: PropTypes.func,
  color: PropTypes.string,
  url: PropTypes.string,
  children: PropTypes.oneOf([PropTypes.array, PropTypes.object]),
};

export default IconWithWord;
